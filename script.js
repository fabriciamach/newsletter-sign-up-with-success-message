const form = document.getElementById("form")
const email =  document.getElementById("email")

form.addEventListener("submit", (event) =>{

        event.preventDefault()
        checkForm()
    }
)

email.addEventListener("blur", () =>{
    checkEmail()
})

function checkEmail(){

    const Useremail = email.value

    if(Useremail === "") {
        errorInput(email)
    }else if(Useremail.includes("@") && Useremail.includes(".com")){
        const formItem = email.parentElement
        formItem.className = "form-container"
    }else{
        errorInput(email)
    }

}

function errorInput(input){
    const formItem = input.parentElement

    formItem.className = "form-container error"
}

function checkForm() {
    checkEmail();

    const formItems = document.querySelectorAll(".form-container");

    const isValid = [...formItems].every(item => {
        return !item.classList.contains("error");
    });

    console.log(isValid);
    if (isValid) {
        window.location.href = "sucess.html"
    }
}

function return_home(){
    window.location.href = "index.html"
}